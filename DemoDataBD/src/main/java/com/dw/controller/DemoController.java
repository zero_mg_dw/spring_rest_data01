/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Product;
import com.dw.repository.ProductRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mmendez
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class DemoController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(
            value = "productos",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Product> getProductos() {
        List<Product> result = (List<Product>) productRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "producto",
            method = RequestMethod.GET,
            produces = "application/json")
    public Product getProduct(@RequestParam("id") Integer id) {
        Product result = productRepository.findOne(id);
        return result;
    }

    @RequestMapping(
            value = "products/page",
            method = RequestMethod.GET,
            produces = "application/json")
    public Page<Product> getProductos(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {

//        Product consulta = new Product();
//        consulta.setName("");
        return productRepository.findAll(new PageRequest(pagenumber, size));

    }

}
