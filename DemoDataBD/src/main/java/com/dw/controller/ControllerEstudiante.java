/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Estudiante;
import com.dw.repository.EstudianteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mmendez
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class ControllerEstudiante {

    @Autowired
    EstudianteRepository estudianteRepository;

    @RequestMapping(
            value = "estudiantes/all",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Estudiante> getAll() {
        List<Estudiante> result 
                = (List<Estudiante>) estudianteRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "estudiante",
            method = RequestMethod.POST,
            produces = "application/json")
    public Estudiante create(@RequestBody Estudiante estudiante) {
        estudiante = estudianteRepository.save(estudiante);
        return estudiante;
    }
}
