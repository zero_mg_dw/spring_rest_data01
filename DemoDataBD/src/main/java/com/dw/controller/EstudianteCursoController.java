/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Estudiante;
import com.dw.entity.EstudianteCurso;
import com.dw.repository.EstudianteCursoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mmendez
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class EstudianteCursoController {

    @Autowired
    EstudianteCursoRepository ecRepository;

    @RequestMapping(
            value = "asignaciones",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<EstudianteCurso> getAll() {
        List<EstudianteCurso> result = (List<EstudianteCurso>) ecRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "asignaciones",
            method = RequestMethod.POST,
            produces = "application/json")
    public EstudianteCurso create(@RequestBody EstudianteCurso estudianteCurso) {
        estudianteCurso = ecRepository.save(estudianteCurso);
        return estudianteCurso;
    }

    @RequestMapping(
            value = "asignaciones/find",
            method = RequestMethod.GET,
            produces = "application/json")

    public Page<EstudianteCurso> get(
            @RequestParam(value = "start", defaultValue = "0") Integer start,
            @RequestParam(value = "length", defaultValue = "10") Integer length,
            @RequestParam(value = "name", defaultValue = "") String name) {

        EstudianteCurso estudianteCurso = null;
        Example<EstudianteCurso> queryExample = null;

        if (name != null && name.trim().length() > 0) {

            estudianteCurso = new EstudianteCurso();
            Estudiante estudiante = new Estudiante();
            estudiante.setName(name);

            estudianteCurso.setIdEstudiante(estudiante);

            ExampleMatcher matcher = ExampleMatcher.matching()
                    .withIgnoreCase()
                    .withMatcher("idEstudiante.name", match -> match.contains());

            queryExample = Example.of(estudianteCurso, matcher);
        }

        if (queryExample == null) {
            return ecRepository.findAll(new PageRequest(start, length));
        } else {
            return ecRepository.findAll(queryExample, new PageRequest(start, length));
        }

    }

}
