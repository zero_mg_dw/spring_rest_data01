/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.repository;

import com.dw.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author mmendez
 */
//public interface ProductRepository 
//        extends JpaRepository<Product, Integer> {
//
//}
public interface ProductRepository 
        extends PagingAndSortingRepository<Product, Integer>, 
        QueryByExampleExecutor<Product> {

}
